﻿using SimpleMvvmToolkit;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Windows;
using System.Windows.Input;
using TransportDemo.Views;

namespace TransportDemo.ViewModels
{
	public class MainVM : ChangeListener
	{
		public MainVM()
		{
			Exit = new RelayCommand(() => Application.Current.Shutdown());
			AddCustomer = new RelayCommand(() => 
			{
 				var dialog = new CustomerWindow{ DataContext = new CustomerVM(), Owner = Owner };
				if(dialog.ShowDialog().GetValueOrDefault())
				{
					SaveCustomer();
				}
			});

			AddManager = new RelayCommand(() =>
			{
				var dialog = new ManagerWindow { DataContext = new ManagerVM(), Owner = Owner };
				if (dialog.ShowDialog().GetValueOrDefault())
				{
					SaveCustomer();
				}
			});

			AddExpeditor = new RelayCommand(() => 
			{
				var dialog = new ExpeditorWindow { DataContext = new ExpeditorVM(), Owner = Owner };
				if(dialog.ShowDialog().GetValueOrDefault())
				{
					SaveExpeditor();
				}
			});
			AddTransport = new RelayCommand(() => 
			{
				var dialog = new TransportWindow { DataContext = new TransportVM(), Owner = Owner };
				if(dialog.ShowDialog().GetValueOrDefault())
				{
					SaveTrnsport();
				}
			});
			Export = new RelayCommand(() => MessageBox.Show("Нужена подложка"));
			Import = new RelayCommand(() => MessageBox.Show("Нужен формат документа"));
			Version = new RelayCommand(() => new AboutWindow { DataContext = new { Description =  string.Format("arise-project.org.ua, Версия {0}", Assembly.GetExecutingAssembly().GetName().Version) }, Owner = Owner } .ShowDialog());
		}
		

		public ICommand Exit { get; set; }

		public ICommand AddCustomer { get; set; }
		
		public ICommand AddManager { get; set; }
		
		public ICommand AddExpeditor { get; set; }

		public ICommand AddTransport { get; set; }

		public ICommand Export { get; set; }

		public ICommand Import { get; set; }

		public ICommand Version { get; set; }

		public Window Owner;


		private void SaveTrnsport()
		{
			
		}

		private void SaveExpeditor()
		{
			
		}

		private void SaveCustomer()
		{
			
		}


		protected override void Unsubscribe()
		{

		}
	}
}
