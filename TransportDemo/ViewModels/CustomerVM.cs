﻿using SimpleMvvmToolkit;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace TransportDemo.ViewModels
{
	public class CustomerVM : ChangeListener
	{
		private string name;

		public string Name
		{
			get { return name; }
			set
			{
				name = value;
				RaisePropertyChanged("Name");
			}
		}

		protected override void Unsubscribe()
		{
			
		}
	}
}
