﻿using SimpleMvvmToolkit;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace TransportDemo.ViewModels
{
	class ExpeditorVM : EntityBaseVM<ExpeditorVM>
	{
		string trackName;

		string postDocument;

		decimal fraxtWithdraw;

		decimal fraxtPayment;

		decimal transportPayment;

		public string TrackName
		{
			get { return trackName; }
			set
			{
				trackName = value;
				RaisePropertyChanged("TrackName");
			}
		}



		public string PostDocument
		{
			get { return postDocument; }
			set
			{
				postDocument = value;
				RaisePropertyChanged("PostDocument");
			}
		}



		public decimal FraxtWithdraw
		{
			get { return fraxtWithdraw; }
			set
			{
				fraxtWithdraw = value;
				RaisePropertyChanged("FraxtWithdraw");
			}
		}



		public decimal FraxtPayment
		{
			get { return fraxtPayment; }
			set
			{
				fraxtPayment = value;
				RaisePropertyChanged("FraxtPayment");
			}
		}


		public decimal TransportPayment
		{
			get { return transportPayment; }
			set
			{
				transportPayment = value;
				RaisePropertyChanged("TransportPayment");
			}
		}



		public CustomerVM Customer { get; set; }
	}
}
