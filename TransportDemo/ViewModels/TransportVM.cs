﻿using SimpleMvvmToolkit;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace TransportDemo.ViewModels
{
	class TransportVM : EntityBaseVM<TransportVM>
	{
		int trackSum;

		decimal updateSum;

		decimal teamSum;

		string trackName;

		decimal oilPayment;

		decimal fraxtWithdraw;

		decimal fraxtPayment;

		public int TrackSum
		{
			get { return trackSum; }
			set
			{
				trackSum = value;
				RaisePropertyChanged("TrackSum");
			}
		}



		public decimal UpdateSum
		{
			get { return updateSum; }
			set
			{
				updateSum = value;
				RaisePropertyChanged("UpdateSum");
			}
		}



		public decimal TeamSum
		{
			get { return teamSum; }
			set
			{
				teamSum = value;
				RaisePropertyChanged("TeamSum");
			}
		}



		public string TrackName
		{
			get { return trackName; }
			set
			{
				trackName = value;
				RaisePropertyChanged("TrackName");
			}
		}



		public decimal OilPayment
		{
			get { return oilPayment; }
			set
			{
				oilPayment = value;
				RaisePropertyChanged("OilPayment");
			}
		}



		public CustomerVM Customer { get; set; }



		public ExpeditorVM Expeditor { get; set; }



		public decimal FraxtWithdraw
		{
			get { return fraxtWithdraw; }
			set
			{
				fraxtWithdraw = value;
				RaisePropertyChanged("FraxtWithdraw");
			}
		}



		public decimal FraxtPayment
		{
			get { return fraxtPayment; }
			set
			{
				fraxtPayment = value;
				RaisePropertyChanged("FraxtPayment");
			}
		}
	}
}
