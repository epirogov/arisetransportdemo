﻿using SimpleMvvmToolkit;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace TransportDemo.ViewModels
{
	public class EntityBaseVM<T> : ChangeListener
	{
		private string name;

		private string comment;

		private DateTime date;

		private string order;

		public string Name
		{
			get { return name; }
			set
			{
				name = value;
				RaisePropertyChanged("Name");
			}
		}



		public string Comment
		{
			get { return comment; }
			set
			{
				comment = value;
				RaisePropertyChanged("Comment");
			}
		}



		public DateTime Date
		{
			get { return date; }
			set
			{
				date = value;
				RaisePropertyChanged("Date");
			}
		}



		public IList<T> History { get; set; }



		public string Order
		{
			get { return order; }
			set
			{
				order = value;
				RaisePropertyChanged("Order");
			}
		}



		protected override void Unsubscribe()
		{

		}
	}
}
