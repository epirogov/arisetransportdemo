﻿using System;
using System.Collections.Generic;
using System.Windows;
using System.Windows.Data;
using System.Windows.Media.Animation;

namespace WPF.Common
{
    /// <span class="code-SummaryComment"><summary>
    /// <span class="code-SummaryComment"></summary>
public class VisibilityAnimation
    {
        public enum AnimationType
        {
            /// <span class="code-SummaryComment"><summary>
            /// <span class="code-SummaryComment"></summary>
           None,

            /// <span class="code-SummaryComment"><summary>
            /// <span class="code-SummaryComment"></summary>
            Fade
        }

        /// <span class="code-SummaryComment"><summary>
        /// <span class="code-SummaryComment"></summary>
        private const int AnimationDuration = 600;

        /// <span class="code-SummaryComment"><summary>
       /// List of hooked objects
        /// <span class="code-SummaryComment"></summary>
        private static readonly Dictionary<FrameworkElement, bool> _hookedElements = 
            new Dictionary<FrameworkElement, bool>();

        /// <span class="code-SummaryComment"><summary>
       /// Get AnimationType attached property
        /// <span class="code-SummaryComment"></summary>
       /// <span class="code-SummaryComment"><param name="obj">Dependency object</param>
        /// <span class="code-SummaryComment"><returns>AnimationType value</returns>
       public static AnimationType GetAnimationType(DependencyObject obj)
        {
            return (AnimationType)obj.GetValue(AnimationTypeProperty);
        }

        /// <span class="code-SummaryComment"><summary>
       /// Set AnimationType attached property
        /// <span class="code-SummaryComment"></summary>
        /// <span class="code-SummaryComment"><param name="obj">Dependency object</param>
        /// <span class="code-SummaryComment"><param name="value">New value for AnimationType</param>
       public static void SetAnimationType(DependencyObject obj, AnimationType value)
        {
            obj.SetValue(AnimationTypeProperty, value);
        }

        /// <span class="code-SummaryComment"><summary>
      /// Using a DependencyProperty as the backing store for AnimationType. 
        /// This enables animation, styling, binding, etc...
        /// <span class="code-SummaryComment"></summary>
      public static readonly DependencyProperty AnimationTypeProperty = 
            DependencyProperty.RegisterAttached(
                "AnimationType",
                typeof(AnimationType),
                typeof(VisibilityAnimation),
                new FrameworkPropertyMetadata(AnimationType.None, 
                    new PropertyChangedCallback(OnAnimationTypePropertyChanged)));

        /// <span class="code-SummaryComment"><summary>
        /// <span class="code-SummaryComment"></summary>
       /// <span class="code-SummaryComment"><param name="dependencyObject">Dependency object</param>
       /// <span class="code-SummaryComment"><param name="e">e</param>
      private static void OnAnimationTypePropertyChanged(
            DependencyObject dependencyObject, 
            DependencyPropertyChangedEventArgs e)
        {
            FrameworkElement frameworkElement = dependencyObject as FrameworkElement;

            if (frameworkElement == null)
            {
                return;
            }

            // If AnimationType is set to True on this framework element, 
            if (GetAnimationType(frameworkElement) != AnimationType.None)
            {
                // Add this framework element to hooked list
                HookVisibilityChanges(frameworkElement);
            }
            else
            {
                // Otherwise, remove it from the hooked list
                UnHookVisibilityChanges(frameworkElement);
            }
        }

        /// <span class="code-SummaryComment"><summary>
        /// <span class="code-SummaryComment"></summary>
       /// <span class="code-SummaryComment"><param name="frameworkElement">Framework element</param>
       private static void HookVisibilityChanges(FrameworkElement frameworkElement)
        {
            _hookedElements.Add(frameworkElement, false);
        }

        /// <span class="code-SummaryComment"><summary>
        /// <span class="code-SummaryComment"></summary>
       /// <span class="code-SummaryComment"><param name="frameworkElement">Framework element</param>
      private static void UnHookVisibilityChanges(FrameworkElement frameworkElement)
        {
            if (_hookedElements.ContainsKey(frameworkElement))
            {
                _hookedElements.Remove(frameworkElement);
            }
        }

        /// <span class="code-SummaryComment"><summary>
        /// <span class="code-SummaryComment"></summary>
       static VisibilityAnimation()
        {
            // Here we "register" on Visibility property "before change" event
            UIElement.VisibilityProperty.AddOwner(
                typeof(FrameworkElement),
                new FrameworkPropertyMetadata(
                    Visibility.Visible, 
                    VisibilityChanged, 
                    CoerceVisibility));
        }

        /// <span class="code-SummaryComment"><summary>
        /// <span class="code-SummaryComment"></summary>
      /// <span class="code-SummaryComment"><param name="dependencyObject">Dependency object</param>
        /// <span class="code-SummaryComment"><param name="e">e</param>
     private static void VisibilityChanged(
            DependencyObject dependencyObject, 
            DependencyPropertyChangedEventArgs e)
        {
            // Ignore
        }

        /// <span class="code-SummaryComment"><summary>
        /// <span class="code-SummaryComment"></summary>
       /// <span class="code-SummaryComment"><param name="dependencyObject">Dependency object</param>
       /// <span class="code-SummaryComment"><param name="baseValue">Base value</param>
      /// <span class="code-SummaryComment"><returns>Coerced value</returns>
        private static object CoerceVisibility(
            DependencyObject dependencyObject, 
            object baseValue)
        {
            // Make sure object is a framework element
            FrameworkElement frameworkElement = dependencyObject as FrameworkElement;
            if (frameworkElement == null)
            {
                return baseValue;
            }

            // Cast to type safe value
            Visibility visibility = (Visibility)baseValue;

            // If Visibility value hasn't change, do nothing.
            // This can happen if the Visibility property is set using data binding 
            // and the binding source has changed but the new visibility value 
            // hasn't changed.
            if (visibility == frameworkElement.Visibility)
            {
                //return baseValue;
            }

            // If element is not hooked by our attached property, stop here
            if (!IsHookedElement(frameworkElement))
            {
                return baseValue;
            }

            // Update animation flag
            // If animation already started, don't restart it (otherwise, infinite loop)
            if (UpdateAnimationStartedFlag(frameworkElement))
            {
                return baseValue;
            }

            // If we get here, it means we have to start fade in or fade out animation. 
            // In any case return value of this method will be Visibility.Visible, 
            // to allow the animation.
            DoubleAnimation doubleAnimation = new DoubleAnimation
            {
                Duration = new Duration(TimeSpan.FromMilliseconds(AnimationDuration))
            };

            // When animation completes, set the visibility value to the requested 
            // value (baseValue)
            doubleAnimation.Completed += (sender, eventArgs) =>
            {
                if (visibility == Visibility.Visible)
                {
                    // In case we change into Visibility.Visible, the correct value 
                    // is already set, so just update the animation started flag
                    UpdateAnimationStartedFlag(frameworkElement);
                }
                else
                {
                    // This will trigger value coercion again 
                    // but UpdateAnimationStartedFlag() function will reture true 
                    // this time, thus animation will not be triggered. 
                    if (BindingOperations.IsDataBound(frameworkElement, 
                        UIElement.VisibilityProperty))
                    {
                        // Set visiblity using bounded value
                        Binding bindingValue = 
                            BindingOperations.GetBinding(frameworkElement, 
                                UIElement.VisibilityProperty);
                        BindingOperations.SetBinding(frameworkElement, 
                            UIElement.VisibilityProperty, bindingValue);
                    }
                    else
                    {
                        // No binding, just assign the value
                        frameworkElement.Visibility = visibility;
                    }
                }
            };

            if (visibility == Visibility.Collapsed || visibility == Visibility.Hidden)
            {
                // Fade out by animating opacity
                //doubleAnimation.From = 1.0;
                //doubleAnimation.To = 0.0;
            }
            else
            {
                // Fade in by animating opacity
                doubleAnimation.From = 0.0;
                doubleAnimation.To = 1.0;
            }

            // Start animation
            frameworkElement.BeginAnimation(UIElement.OpacityProperty, doubleAnimation);

            // Make sure the element remains visible during the animation
            // The original requested value will be set in the completed event of 
            // the animation
            return Visibility.Visible;
        }

        /// <span class="code-SummaryComment"><summary>
        /// <span class="code-SummaryComment"></summary>
      /// <span class="code-SummaryComment"><param name="frameworkElement">Framework element to check</param>
      /// <span class="code-SummaryComment"><returns>Is the framework element hooked?</returns>
       private static bool IsHookedElement(FrameworkElement frameworkElement)
        {
            return _hookedElements.ContainsKey(frameworkElement);
        }

        /// <span class="code-SummaryComment"><summary>
        /// <span class="code-SummaryComment"></summary>
       /// <span class="code-SummaryComment"><param name="frameworkElement">Given framework element</param>
       /// <span class="code-SummaryComment"><returns>Old value of animation started flag</returns>
      private static bool UpdateAnimationStartedFlag(FrameworkElement frameworkElement)
        {
            bool animationStarted = (bool)_hookedElements[frameworkElement];
            _hookedElements[frameworkElement] = !animationStarted;

            return animationStarted;
        }
    }
}