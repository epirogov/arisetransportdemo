USE [Транспортная]
GO

/****** Object:  Table [dbo].[Лицо]    Script Date: 07/17/2013 20:21:21 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[Лицо](
	[ID] [bigint] NOT NULL,
	[Название] [nchar](50) NOT NULL,
	[АдресФактический] [text] NULL,
	[АдресЮридический] [text] NULL,
	[БанковскиеРеквизиты] [text] NULL,
	[Телефон1] [nchar](20) NULL,
	[Телефон2] [nchar](20) NULL,
	[Факс] [nchar](20) NULL,
	[ФормаСобственности] [nchar](10) NULL,
	[ТипЛица] [int] NOT NULL,
 CONSTRAINT [PK_Лицо] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO

