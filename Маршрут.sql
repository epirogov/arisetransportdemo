USE [Транспортная]
GO

/****** Object:  Table [dbo].[Маршрут]    Script Date: 07/17/2013 20:21:34 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[Маршрут](
	[ID] [bigint] NOT NULL,
	[Пункт1] [nchar](40) NULL,
	[Пункт2] [nchar](40) NULL,
	[Расстояние] [float] NULL,
 CONSTRAINT [PK_Маршрут] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]

GO

