USE [Транспортная]
GO

/****** Object:  Table [dbo].[Водитель]    Script Date: 07/17/2013 20:21:06 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[Водитель](
	[ID] [bigint] NOT NULL,
	[ФИО] [nchar](50) NULL,
	[Телефон] [nchar](20) NULL,
 CONSTRAINT [PK_Водитель] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]

GO

