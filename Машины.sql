USE [Транспортная]
GO

/****** Object:  Table [dbo].[Машины]    Script Date: 07/17/2013 20:20:44 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[Машины](
	[ID] [bigint] NOT NULL,
	[IDModel] [bigint] NOT NULL,
	[НомерМашины] [nchar](20) NULL,
	[Информация] [text] NULL,
 CONSTRAINT [PK_Машины] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO

