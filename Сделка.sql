USE [Транспортная]
GO

/****** Object:  Table [dbo].[Сделка]    Script Date: 07/17/2013 20:20:55 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[Сделка](
	[ID] [bigint] NOT NULL,
	[IdЗаказчик] [bigint] NOT NULL,
	[IdПодрядчик] [bigint] NOT NULL,
	[IdВодиель] [bigint] NOT NULL,
	[Почта] [nchar](80) NULL,
	[ДатаСделки] [date] NULL,
	[IDМаршута] [bigint] NOT NULL,
	[Дельта] [float] NULL,
	[IDМенеджер] [bigint] NOT NULL,
	[Фрахт] [float] NULL,
	[ФрахтБезнал] [float] NULL,
	[ДляВодителя] [float] NULL,
	[Счет] [nchar](30) NULL,
	[Информация] [text] NULL,
	[ЧерезКого] [bigint] NULL,
 CONSTRAINT [PK_Сделка] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO

